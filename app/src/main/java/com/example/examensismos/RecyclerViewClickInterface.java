package com.example.examensismos;

import com.example.examensismos.retofit.response.Feature;

public interface RecyclerViewClickInterface {
    void onItemClick(Feature item);

}
