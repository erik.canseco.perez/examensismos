package com.example.examensismos.comunes;

import java.util.ArrayList;
import java.util.List;

public class Constante {

    public static final String MAGNITUD = "magnitud";
    public static final String FECHAINIT = "fechainicial";
    public static final String FECHAFIN = "fechaFinal";

    public static final String LATITUD = "latitud";
    public static final String LONGITUD = "longitud";
    public static final String LUGAR  = "lugar";


    public static final String API_SISMOS_BASE_URL = "https://earthquake.usgs.gov/fdsnws/";


}
