package com.example.examensismos.comunes;

import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Utility {

    public static String twoDigits(int n) {
        return (n<=9) ? ("0"+n) : String.valueOf(n);
    }

    public static boolean comparaFecha(String ini,String fin){
        boolean exitoso = false;
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        try {
            Date intDate = sdf.parse(ini);
            Date finDate = sdf.parse(fin);

            if(finDate.getTime() >= intDate.getTime()){
                exitoso=true;
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return exitoso;
    }

    public static String getTimeStamp(long timeinMillies) {
        String date = null;
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        date = formatter.format(new Date(timeinMillies));
        return date;
    }
}
