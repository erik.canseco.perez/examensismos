package com.example.examensismos.data;


import android.util.Log;
import android.widget.Toast;

import androidx.lifecycle.MutableLiveData;

import com.example.examensismos.comunes.MyApp;
import com.example.examensismos.retofit.SismoServices;
import com.example.examensismos.retofit.SismosCliente;
import com.example.examensismos.retofit.response.ResponseSismos;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SismoRepository {
    private SismosCliente sismosCliente;
    private SismoServices sismoServices;
    private MutableLiveData<ResponseSismos> getSismos;


    public SismoRepository() {
        sismosCliente =SismosCliente.getInstance();
        sismoServices=sismosCliente.getSismoServices();
    }

    public MutableLiveData<ResponseSismos> getSismos(String fechaInicial,String fechaFinal,String magnitud){
        if(getSismos== null){
            getSismos = new MutableLiveData<>();
        }
        Call<ResponseSismos> call = sismoServices.getSismos("geojson",fechaInicial,fechaFinal,magnitud);
        call.enqueue(new Callback<ResponseSismos>() {
            @Override
            public void onResponse(Call<ResponseSismos> call, Response<ResponseSismos> response) {

                if(response.isSuccessful()){
                    getSismos.setValue(response.body());
                }else{
                    Toast.makeText(MyApp.getContext(),"Algo ha ido mal",Toast.LENGTH_LONG).show();
                    Log.e("error ",response.message());
                }
            }

            @Override
            public void onFailure(Call<ResponseSismos> call, Throwable t) {
                Toast.makeText(MyApp.getContext(),"Algo ha ido mal "+t.getMessage(),Toast.LENGTH_LONG).show();
                Log.e("error ",t.getMessage());
            }
        });
        return getSismos;
    }
}
