package com.example.examensismos.data;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.examensismos.retofit.response.ResponseSismos;


public class SismoViewModel extends AndroidViewModel {
    private SismoRepository sismoRepository;
    private LiveData<ResponseSismos> sismos;


    public SismoViewModel(@NonNull Application application) {
        super(application);
        sismoRepository = new SismoRepository();

    }

    public LiveData<ResponseSismos> getSismos(String fechaInicial,String fechaFinal,String magnitud){
        sismos = sismoRepository.getSismos(fechaInicial,fechaFinal,magnitud);
        return sismos;
    }
}
