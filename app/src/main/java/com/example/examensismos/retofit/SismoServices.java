package com.example.examensismos.retofit;

import com.example.examensismos.retofit.response.Feature;
import com.example.examensismos.retofit.response.ResponseSismos;

import retrofit2.Call;
import retrofit2.http.GET;

import retrofit2.http.Path;
import retrofit2.http.Query;

public interface SismoServices {

    //event/1/query?format=geojson&starttime=2014-01-01&endtime=2014-01-02&minmagnitude=5.5

    @GET("event/1/query")
    Call<ResponseSismos> getSismos(@Query("format") String format, @Query("starttime") String fechainit,
                                   @Query("endtime") String fechafin, @Query("minmagnitude") String magnitud);
}
