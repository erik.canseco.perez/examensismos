package com.example.examensismos.retofit;

import com.example.examensismos.comunes.Constante;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SismosCliente {
    private static SismosCliente instance= null;
    private SismoServices sismoServices;
    private Retrofit retrofit;

    public SismosCliente() {
        retrofit=new Retrofit.Builder()
                .baseUrl(Constante.API_SISMOS_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        sismoServices =retrofit.create(SismoServices.class);
    }

    public static SismosCliente getInstance(){
        if(instance == null){
            instance = new SismosCliente();
        }
        return instance;
    }

    public  SismoServices getSismoServices(){
        return sismoServices;
    }
}
