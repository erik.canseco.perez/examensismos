package com.example.examensismos.ui;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import com.example.examensismos.R;
import com.example.examensismos.comunes.Constante;
import com.example.examensismos.comunes.SharedPreferencesManager;
import com.example.examensismos.comunes.Utility;

import java.util.Calendar;

public class MainActivity extends AppCompatActivity {

    EditText etMagnitud;
    EditText etFechaInicial,etfechaFinal;
    Button btBuscar,btBuscarAnterior;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        etMagnitud = findViewById(R.id.sp_magnitud);
        etFechaInicial = findViewById(R.id.etDateInit);
        etfechaFinal = findViewById(R.id.etDatefin);
        btBuscar = findViewById(R.id.bt_buscar);
        btBuscarAnterior = findViewById(R.id.bt_buscar_anterior);

        etFechaInicial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePickerDialog(etFechaInicial);
            }
        });
        etfechaFinal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePickerDialog(etfechaFinal);
            }
        });


        btBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validacion();
            }
        });

        btBuscarAnterior.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                busqueda();
            }
        });


    }

    private void busqueda() {
        if(SharedPreferencesManager.getSomeStringValue(Constante.MAGNITUD)!=null){
            Intent intent= new Intent(MainActivity.this, SismosActivity.class);
            intent.putExtra(Constante.MAGNITUD,SharedPreferencesManager.getSomeStringValue(Constante.MAGNITUD));
            intent.putExtra(Constante.FECHAINIT,SharedPreferencesManager.getSomeStringValue(Constante.FECHAINIT));
            intent.putExtra(Constante.FECHAFIN,SharedPreferencesManager.getSomeStringValue(Constante.FECHAFIN));
            startActivity(intent);
        }else{
            Toast.makeText(this,"No existe una consulta anterior",Toast.LENGTH_LONG).show();
        }
    }


    private void validacion() {
        String magnitud = etMagnitud.getText().toString();

        String[] numeros = magnitud.split("\\.");
        String fechaIni = etFechaInicial.getText().toString();
        String fechaFin = etfechaFinal.getText().toString();
        if(magnitud.isEmpty()){
            etMagnitud.setError("Es necesario este campo");
        }else if(numeros.length == 2 && numeros[1].length()>1){
            etMagnitud.setError("El numero solo puede tener un decimal");
        }else if(fechaIni.isEmpty()){
            etFechaInicial.setError("La fecha Inicial es necesario");
        }else if(fechaFin.isEmpty()){
            etfechaFinal.setError("La fecha fin es necesaria ");
        }else if(!Utility.comparaFecha(fechaIni,fechaFin)){
            Toast.makeText(this,"La fecha fin no puede ser menor a la fecha inicial",Toast.LENGTH_LONG).show();
        }else{
            SharedPreferencesManager.setSomeStringValue(Constante.MAGNITUD,magnitud);
            SharedPreferencesManager.setSomeStringValue(Constante.FECHAINIT,fechaIni);
            SharedPreferencesManager.setSomeStringValue(Constante.FECHAFIN,fechaFin);
            busqueda();
        }

    }

    private void showDatePickerDialog(final  EditText editText) {

        DatePickerFragment newFragment = DatePickerFragment.
                newInstance(new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                final String selectedDate = Utility.twoDigits(day) + "/" + Utility.twoDigits((month+1)) + "/" + year;
                editText.setText(selectedDate);
            }
        });

        newFragment.show(getSupportFragmentManager(), "datePicker");
    }
}
