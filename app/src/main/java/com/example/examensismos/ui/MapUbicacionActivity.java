package com.example.examensismos.ui;

import androidx.core.app.ActivityCompat;
import androidx.fragment.app.FragmentActivity;

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;

import com.example.examensismos.R;
import com.example.examensismos.comunes.Constante;
import com.example.examensismos.retofit.response.Feature;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;

import java.util.List;

public class MapUbicacionActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private Double latitud,longitud;
    private String lugar;
    private FusedLocationProviderClient fusedLocationClient;
    private Location myLocation = null ;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map_ubicacion);
        latitud = getIntent().getDoubleExtra(Constante.LATITUD,0.0);
        longitud = getIntent().getDoubleExtra(Constante.LONGITUD,0.0);
        lugar = getIntent().getStringExtra(Constante.LUGAR);



        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            return;
        }
        fusedLocationClient.getLastLocation()
                .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        // Got last known location. In some rare situations this can be null.
                        if (location != null) {
                            // Logic to handle location object
                            myLocation=location;

                        }
                    }
                });
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        if(myLocation!= null ) {
            crearMarcker(new LatLng(myLocation.getLatitude(), myLocation.getLongitude()));
        }


            LatLng cordenadas = new LatLng( latitud,longitud);

            crearMarcker(cordenadas);

            mMap.moveCamera(CameraUpdateFactory.newLatLng(cordenadas));

    }

    public void crearMarcker(LatLng latLng){
        mMap.addMarker(new MarkerOptions().position(latLng));

    }
}