package com.example.examensismos.ui;

import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.examensismos.R;
import com.example.examensismos.RecyclerViewClickInterface;
import com.example.examensismos.comunes.Utility;
import com.example.examensismos.retofit.response.Feature;

import java.util.List;


public class MysismosRecyclerViewAdapter extends RecyclerView.Adapter<MysismosRecyclerViewAdapter.ViewHolder> {

    private List<Feature> mValues;
    private Context context;
    private RecyclerViewClickInterface recyclerViewClickInterface;



    public MysismosRecyclerViewAdapter(Context context, List<Feature> items,RecyclerViewClickInterface recyclerViewClickInterface) {
        mValues = items;
        this.context = context;
        this.recyclerViewClickInterface = recyclerViewClickInterface;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_sismos, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.magnitud.setText(String.valueOf(holder.mItem.getProperties().getMag()));
        holder.lugar.setText(holder.mItem.getProperties().getPlace());
        holder.hora.setText(Utility.getTimeStamp(holder.mItem.getProperties().getTime()));

        if(holder.mItem.getProperties().getMag()<=4){
            holder.mView.setBackgroundColor(context.getResources().getColor(R.color.verde));

        }else if(holder.mItem.getProperties().getMag()>4 && holder.mItem.getProperties().getMag()<=6){

            holder.mView.setBackgroundColor(context.getResources().getColor(R.color.amarillo));
        }else if(holder.mItem.getProperties().getMag()>6 && holder.mItem.getProperties().getMag()<=7){
            holder.mView.setBackgroundColor(context.getResources().getColor(R.color.naranja));
        }else if(holder.mItem.getProperties().getMag()>7){
            holder.mView.setBackgroundColor(context.getResources().getColor(R.color.rojo));

        }

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recyclerViewClickInterface.onItemClick(mValues.get(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        if(mValues != null){
            return mValues.size();
        }else{
            return 0;
        }
    }

    public void actualizaSismos(List<Feature> items) {
        mValues=items;
        this.notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        public final View mView;
        public final TextView magnitud;
        public final TextView lugar;
        public final TextView hora;
        public Feature mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            magnitud =  view.findViewById(R.id.tv_magnitud);
            lugar = view.findViewById(R.id.tv_lugar);
            hora = view.findViewById(R.id.tv_hora);
        }


        @Override
        public String toString() {
            return super.toString() + " '" + magnitud.getText() + "'";
        }
    }
}
