package com.example.examensismos.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.example.examensismos.R;
import com.example.examensismos.comunes.Constante;

public class SismosActivity extends AppCompatActivity {

    String magnitud,fechainit,fechafin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sismos);
        magnitud=getIntent().getStringExtra(Constante.MAGNITUD);
        fechainit=getIntent().getStringExtra(Constante.FECHAINIT);
        fechafin=getIntent().getStringExtra(Constante.FECHAFIN);

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment,SismosFragment.newInstance(magnitud,fechainit,fechafin))
                .commit();

    }
}
