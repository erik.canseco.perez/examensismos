package com.example.examensismos.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.examensismos.R;
import com.example.examensismos.RecyclerViewClickInterface;
import com.example.examensismos.comunes.Constante;
import com.example.examensismos.data.SismoViewModel;
import com.example.examensismos.retofit.response.Feature;
import com.example.examensismos.retofit.response.ResponseSismos;

import java.util.List;


public class SismosFragment extends Fragment implements RecyclerViewClickInterface {

    private String magnitu,fechaInit,fechaFin;
    private MysismosRecyclerViewAdapter adapter;
    private RecyclerView recyclerView;
    private SismoViewModel viewModel;
    private List<Feature> sismos;
    private SwipeRefreshLayout swipeRefreshLayout;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public SismosFragment() {
    }

    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static SismosFragment newInstance(String magnitud,String fechaInit,String fechaFin) {
        SismosFragment fragment = new SismosFragment();
        Bundle args = new Bundle();
        args.putString(Constante.MAGNITUD,magnitud);
        args.putString(Constante.FECHAINIT,fechaInit);
        args.putString(Constante.FECHAFIN,fechaFin);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        viewModel = ViewModelProviders.of(getActivity()).get(SismoViewModel.class);

        if (getArguments() != null) {
            magnitu = getArguments().getString(Constante.MAGNITUD);
            fechaInit = getArguments().getString(Constante.FECHAINIT);
            fechaFin = getArguments().getString(Constante.FECHAFIN);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sismos_list, container, false);

            Context context = view.getContext();
            recyclerView = view.findViewById(R.id.list);
            swipeRefreshLayout=view.findViewById(R.id.swipeRefresh);

            recyclerView.setLayoutManager(new LinearLayoutManager(context));
            adapter = new MysismosRecyclerViewAdapter(getActivity(),sismos,this);

            recyclerView.setAdapter(adapter);
            loadSismosData();

            swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    loadSismosData();
                }
            });
        return view;
    }

    private void loadSismosData() {
            viewModel.getSismos(fechaInit,fechaFin,magnitu).observe(getActivity(), new Observer<ResponseSismos>() {
                @Override
                public void onChanged(ResponseSismos responseSismos) {
                    sismos=responseSismos.getFeatures();
                    adapter.actualizaSismos(responseSismos.getFeatures());
                    if(swipeRefreshLayout!=null);
                    {
                        swipeRefreshLayout.setRefreshing(false);
                    }
                }

            });
    }

    @Override
    public void onItemClick(Feature item) {

        Log.e("regreso",item.getGeometry().getCoordinates().get(0).toString());

        Intent i = new Intent(getActivity(),MapUbicacionActivity.class);
        i.putExtra(Constante.LATITUD,item.getGeometry().getCoordinates().get(1));
        i.putExtra(Constante.LONGITUD,item.getGeometry().getCoordinates().get(0));
        i.putExtra(Constante.LUGAR,item.getProperties().getPlace());
        getActivity().startActivity(i);
    }
}
